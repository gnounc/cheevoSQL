<?php

function getAchievements(field1, field2, field3, field4)
{
	$idnums = array();

	for ($idx = 0, $x = 0; $x < 80; $x++)
	{
		if(x < 20)
		{
			if(field1 & pow(x, 2))
			{
			    $idnums[idx] = x;
			    idx++;
			}
			continue;
		}

		if(x < 40)
		{
			if(field2 & pow(x - 20, 2))
			{
			    $idnums[idx] = x;
			    idx++;
			}
			continue;
		}

		if(x < 60)
		{
			if(field3 & pow(x - 40, 2))
			{
			    $idnums[idx] = x;
			    idx++;
			}
			continue;
		}

		if(x < 80)
		{
			if(field4 & pow(x - 60, 2))
			{
			    $idnums[idx] = x;
			    idx++;
			}
			continue;
		}
	}

	return $idnums;
};

?>
